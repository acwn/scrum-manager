package sm_handler

import (
	"github.com/prometheus/client_golang/prometheus"
)

type s_metrics struct {
	statuscode *prometheus.CounterVec
	methodcode *prometheus.CounterVec
}
