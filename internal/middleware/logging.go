package middleware

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	statusCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "sm_http_status_codes_total",
			Help: "Total number of HTTP status codes",
		},
		[]string{"code"},
	)

	methodCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "sm_http_method_total",
			Help: "Total number of HTTP methods",
		},
		[]string{"method"},
	)

	durationHistogram = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "sm_http_request_duration_seconds",
			Help:    "Histogram of response latency",
			Buckets: []float64{.25, .5, 1, 2.5, 5, 10},
		},
		[]string{"method", "path"},
	)
)

func init() {
	prometheus.MustRegister(statusCounter)
	prometheus.MustRegister(methodCounter)
	prometheus.MustRegister(durationHistogram)
}

type wrappedWriter struct {
	http.ResponseWriter
	statusCode int
}

func (w *wrappedWriter) WriteHeader(statusCode int) {
	w.statusCode = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		wrapped := &wrappedWriter{
			ResponseWriter: w,
			statusCode:     http.StatusOK,
		}

		next.ServeHTTP(wrapped, r)
		statusCounter.WithLabelValues(fmt.Sprintf("%d", wrapped.statusCode)).Inc()
		methodCounter.WithLabelValues(r.Method).Inc()
		durationHistogram.WithLabelValues(r.Method, r.URL.Path).Observe(time.Since(start).Seconds())
		log.Println(wrapped.statusCode, r.Method, r.URL.Path, time.Since(start))
	})
}

func MetricsHandler() http.Handler {
	return promhttp.Handler()
}
